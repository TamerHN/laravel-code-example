<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContinentsController extends Controller
{
    //

    public function index()
    {
      // TODO: Make sure the user is authonticated to retreive the data

      return \App\Continent::get();
    }
}
