<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;

class CountryController extends Controller
{

    // TODO: Authonticate the user
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($continent_id)
    {
        //get countires in the given continent
        return \App\Country::where('continent_id', $continent_id)
                           ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CountryRequest $request, $continent_id)
    {
        //add a new country to the given continent id
        $data = $request->all();

        $continent = \App\Continent::find($continent_id);
        $continent->Countries()->create($data);
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($continent_id, $country_id)
    {
        /**
         * Get a country by its id, and make sure to insert the continent id
         *  with the country for security reasons
         */
        return \App\Country::where('continent_id', $continent_id)
                           ->findOrFail($country_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CountryRequest $request, $continent_id, $country_id)
    {
        //
        $data = $request->all();
        //get the country in the continent
        $country = \App\Country::where('continent_id', $continent_id)
                               ->find($country_id);
        $country->fill($data);
        $country->save();
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($continent_id, $country_id)
    {
        $country =  \App\Country::where('continent_id', $continent_id)
                                ->findOrFail($country_id);
        //check if the country has cities, don't delete it
        $validation = Validator::make(['country' => $country_id], [
          'country' => 'related:\App\City,country_id'
        ]);
        if($validation->fails())
          return response()->json($validation->errors())
                           ->setStatusCode(422);
        $country->delete();
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }
}
