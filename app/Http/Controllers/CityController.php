<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;

class CityController extends Controller
{

    // TODO: Authonticate the user
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($continent_id, $country_id)
    {
        /**
         * Get the cities in a country, and make sure to insert the continent id
         *  with the country for security reasons,
         * Also return the Neighborhoods with the cities
         */
        return \App\City::whereHas('Country', function ($query) use($continent_id, $country_id)
                        {
                          $query->where('continent_id', $continent_id)
                                ->where('id', $country_id);
                        })
                        ->with('Neighborhoods')
                        ->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CityRequest $request, $continent_id, $country_id)
    {
        /**
         * Get a city by its id, and make sure to insert the country id and
         *  the continent id for security reasons
         */
        $data = $request->all();
        //find the country and add the city
        $country = \App\Country::find($country_id);
        $country->Cities()->create($data);
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($continent_id, $country_id, $city_id)
    {
        //
        return \App\City::whereHas('Country', function ($query) use($continent_id, $country_id)
                        {
                          $query->where('continent_id', $continent_id)
                                ->where('id', $country_id);
                        })
                        ->with('Neighborhoods')
                        ->findOrFail($city_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CityRequest $request, $continent_id, $country_id, $city_id)
    {
        //
        $data = $request->all();

        //get the City
        $city = \App\City::find($city_id);
        $city->fill($data);
        $city->save();
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($continent_id, $country_id, $city_id)
    {
        $city = \App\City::whereHas('Country', function ($query) use($continent_id)
                          {
                            $query->where('continent_id', $continent_id);
                          })
                         ->findOrFail($city_id);

        //check if the city has Neighborhoods, don't delete it
        $validation = Validator::make(['city' => $city_id], [
          'city' => 'related:\App\Neighborhood,city_id'
        ]);
        if($validation->fails())
          return response()->json($validation->errors())
                           ->setStatusCode(422);
        $city->delete();
        return response()->json(['message' => trans('system.success')])
                         ->setStatusCode(200);
    }
}
