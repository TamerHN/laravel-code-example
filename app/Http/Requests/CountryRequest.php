<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CountryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Authorize the user
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //get the segments to get the country id
        $seg = $this->segments();
        $update = $this->method() == 'PUT' ? 1 : 0;
        $country_id = -1;
        //if update method, don't check for uniqueness of the old values
        if($update)
        {
          $country = \App\Country::where('continent_id', $seg[3])
                                 ->findOrFail($seg[5]);
          $country_id = $country->id;
        }

        return [
            'name'     => 'required|string|unique:countries,name,'.$country_id,
            'dial_key' => 'required|numeric|unique:countries,dial_key,'.$country_id
        ];
    }

    public function wantsJson()
    {
      return true;
    }
}
