<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      //get the segments to get the country id
      $seg = $this->segments();
      $update = $this->method() == 'PUT' ? 1 : 0;
      $city_id = -1;
      //if update method, don't check for uniqueness of the old values
      if($update)
      {
        $city = \App\City::whereHas('Country', function ($query) use($seg)
                          {
                            $query->where('continent_id', $seg[3]);
                          })
                         ->findOrFail($seg[7]);
        $city_id = $city->id;
      }

      return [
          'name' => 'required|string|unique:cities,name,'.$city_id,
      ];
    }

    public function wantsJson()
    {
      return true;
    }
}
