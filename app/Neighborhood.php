<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    //
    protected $fillable = ['name', 'city_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function City()
    {
      return $this->belongsTo('\App\City');
    }

}
