<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = ['name', 'country_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function Country()
    {
      return $this->belongsTo('\App\Country');
    }

    public function Neighborhoods()
    {
      return $this->hasMany('\App\Neighborhood');
    }
}
