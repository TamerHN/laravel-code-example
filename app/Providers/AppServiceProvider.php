<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      //check if the record exists in a table
      Validator::extend('exists', function ($attributes, $value, $parameters)
      {
        return $parameters[0]::find($value);
      }, trans('validation.notExist'));

      //check if the record exists in a table
      Validator::extend('related', function ($attributes, $value, $parameters)
      {
        return !$parameters[0]::where($parameters[1], $value)
                             ->first();
      }, trans('validation.noDelete'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
