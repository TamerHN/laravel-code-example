<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{
    //
    protected $fillable = ['name'];
    protected $hidden = ['created_at', 'updated_at'];

    public function Countries()
    {
      return $this->hasMany('\App\Country');
    }

    public function Cities()
    {
      return $this->hasManyThrough('\App\City','\App\Country');
    }
}
