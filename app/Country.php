<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = ['name', 'dial_key', 'continent_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function Continent()
    {
      return $this->belongsTo('\App\Continent');
    }

    public function Cities()
    {
      return $this->hasMany('\App\City');
    }

    public function Neighborhoods()
    {
      return $this->hasManyThrough('\App\Neighborhood', '\App\City');
    }
}
