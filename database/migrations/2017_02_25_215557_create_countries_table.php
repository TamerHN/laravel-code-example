<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('countries', function (Blueprint $table)
        {
          $table->increments('id');
          $table->string('name');
          $table->integer('dial_key');
          $table->integer('continent_id')
                ->unsigned();
          $table->foreign('continent_id')
                ->references('id')
                ->on('continents');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('countries');
    }
}
