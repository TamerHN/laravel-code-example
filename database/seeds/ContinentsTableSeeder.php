<?php

use Illuminate\Database\Seeder;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //the seven continents of the world
        $continents = [['name' => 'Asia'], ['name' => 'Africa'], ['name' => 'Antarctica'],
                       ['name' => 'Australia'], ['name' => 'Europe'], ['name' => 'North America'],
                       ['name' => 'South America']];

        DB::table('continents')
          ->insert($continents);
    }
}
